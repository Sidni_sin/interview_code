<?php

namespace App\Services\TokenScansApi\ScanInterface;

use App\Services\TokenScansApi\Dto\TokenBalanceRequestDto;
use App\Services\TokenScansApi\Dto\TokenBalanceResponseDto;

interface ScanInterface
{
    public function tokenAccountBalanceByContractAddress(TokenBalanceRequestDto $requestDto): TokenBalanceResponseDto;
    public function urlApi(): string;
    public function apiKey(): ?string;
    public function fieldForApiKey(): string;
    public function sendGetRequest(array $options = []): array;

}