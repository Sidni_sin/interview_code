<?php

namespace App\Actions;

use App\Helpers\TokenScansApiHelper;
use App\Helpers\UnitConverter;
use App\Services\TokenScansApi\BscscanApi;
use App\Services\TokenScansApi\Dto\TokenBalanceRequestDto;
use App\Services\TokenScansApi\EtherscanApi;
use App\Services\TokenScansApi\VelasApi;

class GetActualUserBalanceAction implements Action
{
    public function __construct(
        protected readonly UnitConverter $unitConverter,
        protected readonly BscscanApi $bscscanApi,
        protected readonly EtherscanApi $etherscanApi,
        protected readonly VelasApi $velasApi
    ) { }

    public function handler(): string
    {
        $velas      = TokenScansApiHelper::addressesVelas();
        $bscScans   = TokenScansApiHelper::addressesBscScans();
        $etherscans = TokenScansApiHelper::addressesEtherscans();
        $total      = TokenScansApiHelper::TOTAL_SUPPLY;

        foreach ($bscScans as $bscScan)
        {
            $response = $this->bscscanApi->tokenAccountBalanceByContractAddress(new TokenBalanceRequestDto(...$bscScan));
            $unit = $this->unitConverter->fromWei($response->balance);
            $total = bcsub($total, $unit, 18);
        }

        foreach ($etherscans as $etherscan)
        {
            $response = $this->etherscanApi->tokenAccountBalanceByContractAddress(new TokenBalanceRequestDto(...$etherscan));
            $unit = $this->unitConverter->fromWei($response->balance);
            $total = bcsub($total, $unit, 18);
        }

        foreach ($velas as $vela)
        {
            $response = $this->velasApi->tokenAccountBalanceByContractAddress(new TokenBalanceRequestDto(...$vela));
            $unit = $this->unitConverter->fromWei($response->balance);
            $total = bcsub($total, $unit, 18);
        }

        return $total;
    }
}
