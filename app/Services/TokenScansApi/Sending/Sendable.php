<?php

namespace App\Services\TokenScansApi\Sending;

interface Sendable
{
    public function get(string $url, array $options = []);
}