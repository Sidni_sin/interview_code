<?php

namespace App\Providers;

use App\Actions\GetActualUserBalanceAction;
use App\Controllers\UserBalanceController;
use App\Fakes\FakeClient;
use App\Services\TokenScansApi\Sending\Sendable;
use App\Services\TokenScansApi\Sending\Client;
use GuzzleHttp\ClientInterface;

class BindProvider implements Provider
{
    public function bind(): void
    {
        app()->set(GetActualUserBalanceAction::class, \DI\autowire());
        app()->set(UserBalanceController::class, \DI\autowire());
        app()->set(Sendable::class, fn() => app(Client::class));
        app()->set(ClientInterface::class, fn() => new FakeClient());
    }
}