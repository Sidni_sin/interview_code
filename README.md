# interview code

# needed

php >= 8.2

composer

# installing

```bash
composer install
```

# running

```bash
php -S localhost:8001
```


Open the project `http://localhost:8001/get-actual-balance`
[link](http://localhost:8001/get-actual-balance)



