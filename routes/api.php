<?php

use App\Controllers\UserBalanceController;
use App\Kernel\Routes\Route;

Route::get('get-actual-balance', [UserBalanceController::class, 'getActualBalance']);