<?php

namespace App\Services\TokenScansApi;

use App\Services\TokenScansApi\Dto\TokenBalanceRequestDto;
use App\Services\TokenScansApi\Dto\TokenBalanceResponseDto;

class VelasApi extends TokenScanAbstract
{
    public function tokenAccountBalanceByContractAddress(TokenBalanceRequestDto $requestDto): TokenBalanceResponseDto
    {
        $response = $this->sendGetRequest([
            'query' => [
                'module' => 'account',
                'action' => 'tokenbalance',
                'contractaddress' => $requestDto->contractAddress,
                'address' => $requestDto->address,
            ]
        ]);

        return new TokenBalanceResponseDto(...[
            'balance' => $response['result'],
        ]);
    }

    public function urlApi(): string
    {
        //config('velas.url');
        return 'velas';
    }

    public function apiKey(): ?string
    {
        //config('velas.key');
        return 'bla_key';
    }
}