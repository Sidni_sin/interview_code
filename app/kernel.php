<?php

use App\Providers\BindProvider;
use App\Providers\RouteProvider;
use DI\ContainerBuilder;

require_once 'vendor/autoload.php';

(new BindProvider())->bind();
(new RouteProvider())->bind();

function app($make = null)
{
    static $container;

    if (!$container) {
        $containerBuilder = new ContainerBuilder();
        $container = $containerBuilder->build();
    }

    if ($make) {
        return $container->get($make);
    }

    return $container;
}