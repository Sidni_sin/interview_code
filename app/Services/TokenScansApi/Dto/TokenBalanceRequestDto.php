<?php

namespace App\Services\TokenScansApi\Dto;

class TokenBalanceRequestDto
{
    public function __construct(
        public string $address,
        public string $contractAddress
    ) {}
}