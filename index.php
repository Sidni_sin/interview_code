<?php

use App\Kernel\Routes\RouteRegister;

require_once 'app/kernel.php';

echo app(RouteRegister::class)->handleRequest();
