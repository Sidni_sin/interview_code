<?php

namespace App\Services\TokenScansApi;

use App\Services\TokenScansApi\ScanInterface\ScanInterface;
use App\Services\TokenScansApi\Sending\Sendable;

abstract class TokenScanAbstract implements ScanInterface
{
    public function __construct(
        protected Sendable $client,
    ) { }

    public function sendGetRequest(array $options = []): array
    {
        if($this->apiKey()){
            $options['query'][$this->fieldForApiKey()] = $this->apiKey();
        }

        return $this->client->get($this->urlApi(), $options);
    }

    public function fieldForApiKey(): string
    {
        return 'apikey';
    }
}