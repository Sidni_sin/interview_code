<?php

namespace App\Providers;


class RouteProvider implements Provider
{
    public function bind(): void
    {
        require __DIR__ . '/../../routes/api.php';
    }
}