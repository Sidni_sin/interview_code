<?php

namespace App\Controllers;

use App\Actions\GetActualUserBalanceAction;

class UserBalanceController
{
    public function __construct(
        private readonly GetActualUserBalanceAction $actualUserBalanceAction,
    ){}

    public function getActualBalance()
    {
        return json_encode([
            'balance' => $this->actualUserBalanceAction->handler()
        ]);
    }
}