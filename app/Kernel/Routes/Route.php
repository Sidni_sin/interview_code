<?php

namespace App\Kernel\Routes;

/**
 * @method static get(string $routeName, array $handler)
*/
class Route
{
    public static function __callStatic($method, $args)
    {
        $instance = app(RouteRegister::class);

        return call_user_func_array([$instance, $method], $args);
    }
}