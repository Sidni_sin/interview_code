<?php

namespace App\Services\TokenScansApi\Dto;

class TokenBalanceResponseDto
{
    public function __construct(
        public string $balance
    ) {}
}