<?php

namespace App\Services\TokenScansApi;

use App\Services\TokenScansApi\Dto\TokenBalanceRequestDto;
use App\Services\TokenScansApi\Dto\TokenBalanceResponseDto;

class BscscanApi extends TokenScanAbstract
{
    public function tokenAccountBalanceByContractAddress(TokenBalanceRequestDto $requestDto): TokenBalanceResponseDto
    {
        $response = $this->sendGetRequest([
            'query' => [
                'module' => 'account',
                'action' => 'tokenbalance',
                'contractaddress' => $requestDto->contractAddress,
                'address' => $requestDto->address,
                'tag' => 'latest',
            ]
        ]);

        return new TokenBalanceResponseDto(...[
            'balance' => $response['result'],
        ]);
    }

    public function urlApi(): string
    {
        //config('bscscan.url');
        return 'bscscan';
    }

    public function apiKey(): ?string
    {
        //config('bscscan.key');
        return 'api_key';
    }
}