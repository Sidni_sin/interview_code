<?php

namespace App\Helpers;

class TokenScansApiHelper
{
    public const TOTAL_SUPPLY = '100000000';

    public static function addressesBscScans(): array
    {
        return [
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ],
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ],
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ],
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ],
        ];
    }

    public static function addressesEtherscans(): array
    {
        return [
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ]
        ];
    }

    public static function addressesVelas(): array
    {
        return [
            [
                'address' => '0xBlaBla',
                'contractAddress' => '0xBlaBla',
            ]
        ];
    }
}