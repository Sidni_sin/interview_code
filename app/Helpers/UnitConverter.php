<?php

namespace App\Helpers;

class UnitConverter
{
    public function fromWei(string $wei): string
    {
        return bcdiv($wei,1000000000000000000,18);
    }
}