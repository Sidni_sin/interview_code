<?php

namespace App\Kernel\Routes;

class RouteRegister
{
    protected array $routes = [];

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function register($method, $uri, $action): void
    {
        $this->routes[strtoupper($method)][$uri] = $action;
    }

    public function get($uri, $action): void
    {
        $this->register('GET', ltrim($uri, '/'), $action);
    }

    public function handleRequest()
    {
        $requestUri = ltrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        $requestMethod = $_SERVER['REQUEST_METHOD'];

        if (isset($this->routes[$requestMethod][$requestUri])) {
            $action = $this->routes[$requestMethod][$requestUri];

            $controller = app($action[0]);

            return $controller->{$action[1]}();
        }

        header("HTTP/1.1 404 Not Found");
        return "404 Not Found";
    }
}