<?php

namespace App\Services\TokenScansApi\Sending;

use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

class Client implements Sendable
{
    public function __construct(
        protected ClientInterface $client,
    ) { }

    public function get(string $url, array $options = [])
    {
        $client = clone $this->client;

        try {
            $response = $client->get($url, $options);
        } catch (ClientException $e) {
            $errorType = 'ClientException';
            $data = $e->__toString();
            $this->exceptionHandler($url, $errorType, $data);
            die;
        } catch (GuzzleException $e) {
            $errorType = 'GuzzleException';
            $data = $e->__toString();
            $this->exceptionHandler($url, $errorType, $data);
            die;
        } catch (Exception $e) {
            $errorType = 'Exception';
            $data = $e->__toString();
            $this->exceptionHandler($url, $errorType, $data);
            die;
        }

        $response = json_decode($response->getBody()->getContents(), true);

        if (!$response['status']) {
            $errorType = 'SuccessFalse';
            $data = "Response:\n" . print_r($response, true);
            $this->exceptionHandler($url, $errorType, $data);
            die;
        }

        return $response['response'];
    }

    protected function exceptionHandler($uri, $errorType, $data): void
    {
        // $notifier = substr(static::class, strrpos(static::class, '\\') + 1);
        // NotifierErrorsLoggerFacade::log($notifier, 'GET', $uri, $errorType, $data);
    }
}