<?php

namespace App\Fakes;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Utils;

class FakeClient implements ClientInterface
{
    public function get($uri, array $options = []): ResponseInterface
    {
        $body = Utils::streamFor(json_encode([
            'status'    => 200,
            'response'  => [
                'fake'      => 'true',
                'result'    => $this->urlOfValue()[$uri] ?? '',
            ],
        ]));

        return new Response(200, [], $body);
    }

    private function urlOfValue(): array
    {
        return [
            'velas'     => 10000000,
            'bscscan'   => 10000,
            'etherscan' => 3,
        ];
    }

    public function send(RequestInterface $request, array $options = []): ResponseInterface
    {
        // TODO: Implement send() method.
    }

    public function sendAsync(RequestInterface $request, array $options = []): PromiseInterface
    {
        // TODO: Implement sendAsync() method.
    }

    public function request(string $method, $uri, array $options = []): ResponseInterface
    {
        // TODO: Implement request() method.
    }

    public function requestAsync(string $method, $uri, array $options = []): PromiseInterface
    {
        // TODO: Implement requestAsync() method.
    }

    public function getConfig(string $option = null)
    {
        // TODO: Implement getConfig() method.
    }
}
