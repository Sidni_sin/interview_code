<?php

namespace App\Services\TokenScansApi;

use App\Services\TokenScansApi\Dto\TokenBalanceRequestDto;
use App\Services\TokenScansApi\Dto\TokenBalanceResponseDto;

class EtherscanApi extends TokenScanAbstract
{
    public function tokenAccountBalanceByContractAddress(TokenBalanceRequestDto $requestDto): TokenBalanceResponseDto
    {
        $response = $this->sendGetRequest([
            'query' => [
                'module' => 'account',
                'action' => 'tokenbalance',
                'contractaddress' => $requestDto->contractAddress,
                'address' => $requestDto->address,
                'tag' => 'latest',
            ]
        ]);

        return new TokenBalanceResponseDto(...[
            'balance' => $response['result'],
        ]);
    }

    public function urlApi(): string
    {
        //config('etherscan.url');
        return 'etherscan';
    }

    public function apiKey(): ?string
    {
        //config('etherscan.key');
        return 'api_key';
    }
}